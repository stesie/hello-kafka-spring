package de.brokenpipe.hellokafkaspring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ScheduledPublisher {
    private static Logger logger = LoggerFactory.getLogger(ScheduledPublisher.class);

    @Autowired
    private KafkaTemplate<String, String> template;


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 3500)
    public void reportCurrentTime() {
        final String formattedDate = dateFormat.format(new Date());
        logger.info("The time is now {}", formattedDate);

        template.send("testchen", "aktuelle Zeit: " + formattedDate);
    }
}
