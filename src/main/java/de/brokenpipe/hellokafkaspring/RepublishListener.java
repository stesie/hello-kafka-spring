package de.brokenpipe.hellokafkaspring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class RepublishListener {
    private static Logger logger = LoggerFactory.getLogger(RepublishListener.class);

    @Autowired
    private KafkaTemplate<String, String> template;

    @KafkaListener(topics = "republished")
    public void listen(@Payload String message) {
        logger.info(message);
    }
}
