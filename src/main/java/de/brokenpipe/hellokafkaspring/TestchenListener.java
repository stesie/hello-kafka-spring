package de.brokenpipe.hellokafkaspring;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
class TestchenListener {
    private static Logger logger = LoggerFactory.getLogger(TestchenListener.class);

    @Autowired
    private KafkaTemplate<String, String> template;

    @KafkaListener(topics = "testchen")
    public void listen(ConsumerRecord<?, String> cr) {

        if (cr.value().startsWith("aktuelle Zeit")) {
            this.template.send("republished", cr.value().substring(15));
        }

        logger.info(cr.value() + " auf Partition-ID: " + cr.partition());
    }
}
