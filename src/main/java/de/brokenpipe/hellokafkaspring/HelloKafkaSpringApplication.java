package de.brokenpipe.hellokafkaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HelloKafkaSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloKafkaSpringApplication.class, args);
    }
}
